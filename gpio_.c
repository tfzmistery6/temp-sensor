#include "stm32f10x.h"

void GPIO_init(void)
{
	RCC -> APB2ENR |= RCC_APB2ENR_IOPCEN; //Включим тактирование порта С
	
	//Настроим PC1 PC2 PC3 как входы датчиков холла 
	GPIOC -> CRL &= ~(1 << 6 | 1 << 10 | 1 << 14); //Сбросим ненужные еденички
	GPIOC -> CRL |= (1 << 7 | 1 << 11 | 1 << 15); //включим режим входа с подтяжкой
	GPIOC -> ODR |= (GPIO_ODR_ODR1 | GPIO_ODR_ODR2 | GPIO_ODR_ODR3); //Укажем подтяжку к плюсу
	
}

